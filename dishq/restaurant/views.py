from django.shortcuts import render,redirect
from django.http import HttpResponseBadRequest, HttpResponse, JsonResponse
from selenium import webdriver
from bs4 import BeautifulSoup
# Create your views here.
import time

# Wait for 5 seconds

# Function to Scrap Restaurant Data
def restaurant_details(driver,url):
    data = {}
    reviews_list = []
    _lat = ''
    _long = ''
    html = driver.page_source
    soup = BeautifulSoup(html,'lxml')
    reviews = soup.findAll("div", {"class": "review-post"})
    for review in reviews:
        review = review.find("span",{"class":"more"}).text
        print review
        reviews_list.append(review)
    res_location = soup.findAll("div", {"class": "address-wrap"})
    for loc in res_location:
         lat_long = loc.find('a').get('href')
         _lat = lat_long.split('q=')[1].split(",")[0]
         _long = lat_long.split('q=')[1].split(",")[1]

    data['complete_address'] = soup.find("span", {"class": "address-info"}).text
    data['reviews'] = reviews_list
    data['restaurant_name'] = soup.find("h1", {"class": "restnt-name"}).text
    data['lat'] = _lat
    data['long'] = _long
    print data
    return data


# Fetch And Scrapes all restaurants
def restaurant_list_scraper(driver,url,page_no):
    restaurants = []
    el = driver.find_element_by_xpath('//*[@id="w1-pagination"]/li['+str(page_no)+']/a')
    el.click()
    time.sleep(2)
    html = driver.page_source
    soup = BeautifulSoup(html,'lxml')
    result = soup.findAll("div", {"class": "cardBg"})
    for i in result:
        data = {}
        data['restaurant_name'] = i.find("a", {"data-type": "Name"}).text
        data['location_locality'] = i.find("a", {"data-type": "Locality"}).text
        data['location_area'] = i.find("a", {"data-type": "Area"}).text
        data['restaurant_url'] =  i.find("a", {"data-type": "Name"}).get('href')
        data['rating'] = i.find("div", {"class": "rating"}).span.text or 0.00
        restaurants.append(data)
    return restaurants


# Home Page View
def home(request):
    _city = str(request.GET.get('city'))
    if(_city == 'None'):
        return render(request,'home.html',{})
    else:
        return redirect('/restaurant?city='+_city+'&page_index=2')



# Function to render restaurant detail and map
def detail(request):
    _page_url = str(request.GET.get('url'))
    print _page_url.split("'")[1]
    if _page_url == 'None':
        print "Hello"
        return JsonResponse({"error":'404'})

    abs_page_url = 'https://www.dineout.co.in'+_page_url.split("'")[1]
    driver = webdriver.Chrome()
    driver.get(abs_page_url)
    res_data = restaurant_details(driver,abs_page_url)
    driver.quit()
    return render(request,'detail.html',{'restaurant_data':res_data})





def restaurant(request):
    context = {}
    city = 'delhi'
    try:
        page_index = 2
        city = request.GET['city']
        page_index = request.GET['page_index']
        context['city'] = city
        context['page_index'] = page_index
    except:
        context['city'] = city
        context['page_index'] = 2

    driver = webdriver.Chrome()
    url = 'https://www.dineout.co.in/'+city+'-restaurants'
    driver.get(url)
    restaurants = restaurant_list_scraper(driver,url,page_index)
    driver.quit()

    context['restaurants'] = restaurants
    print context
    return render(request,'restaurant.html',context)
