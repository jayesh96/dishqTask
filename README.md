# Restaurant Scraper


Restaurant Scraper is a basic scraping tool used to scrape restaurant details from Dineout Website [www.dineout.co.in]

The Languages and Frameworks Used Are
  - Python
  - Django(Python Web Framework)
  - HTML
  - CSS
  - Javascript
  - Bootstrap(JS Ui Framework)
  
Libraries Used are
  - Selenium
  - Beautiful Soup



# Installation 

    git init
    
    git clone https://gitlab.com/jayesh96/ds.git
  
# Activate the Virtualenv
    source env/bin/activate

# Install the packages
    pip install -r requirements.txt

# Start the Server
    python manage.py runserver 0.0.0.0:8000

